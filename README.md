CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Phone Registration allow users to register and login using only a phone number.
Users can then log-in using their phone number and password for authentication.

INSTALLATION
------------

Install the Phone Registration module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

REQUIREMENTS
-------------

* This module requires no modules outside of Drupal core.

CONFIGURATION
-------------

* This module requires no configuration.
* Recommend to clear Drupal cache.

MAINTAINERS
-----------

Current maintainers:
 * Sandeep Jangra - https://www.drupal.org/u/sandeep_jangra
