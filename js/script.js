/**
 * @file
 * Disable and hide email verfication checkbox when we enable the phone number.
 * registration.
 */
(function($, Drupal) {
  var $emailVerfication = $(".form-item-user-email-verification");
	var $checkPhone = $("#edit-phone-numbers");

	$checkPhone.on('#edit-phone-number-register', 'click', function(event) {
		if ($(this). prop("checked") === true) {
			$("#edit-user-email-verification"). prop("checked", false);
			$emailVerfication.hide();
		}
		else {
			$emailVerfication.show();
		}
    // Prevents the default event behavior (ie: click).
    event.preventDefault();
    // Prevents the event from propagating (ie: "bubbling").
    event.stopPropagation();
  });

})(jQuery, Drupal);
